'use strict';
const assert = require('assert');
const {
    parseAuthHeader,
    signGET,
    signHEAD,
    signPOST,
    signPUT,
    headerKey
} = require('..');
const apiKey = '134654',
    time = 1518619891,
    url = 'shoptimiza.com/hola',
    bodySignature = 'SCTDLGCDRCTHNTH',
    secret = 'secret';
describe('#headerKey', function () {
    it('should be X-Shoptimiza-Auth', function () {
        assert.deepEqual(headerKey, 'X-Shoptimiza-Auth');
    });
});
describe('#signGET(apiKey, time, url, secret)', function () {
    it('should return signature', function () {
        var actual = signGET(apiKey, time, url, secret);
        const expected = '134654.1518619891.SOvueGN73WAkfa6DguAZ7EEHKaMczZUsjdtKWfsK/r0=';
        assert.deepEqual(actual, expected);
    });
});
describe('#signHEAD(apiKey, time, url, secret)', function () {
    it('should return signature', function () {
        var actual = signHEAD(apiKey, time, url, secret);
        var expected = '134654.1518619891.iL1udpe1sHDzj3eGfK8HKUk/00GXt1eV812w9nHjwTU='
        assert.deepEqual(actual, expected);
    });
});
describe('#signPOST(apiKey, time, url, bodySignature, secret)', function () {
    it('should return signature', function () {
        var actual = signPOST(apiKey, time, url, bodySignature, secret);
        var expected = '134654.1518619891.SCTDLGCDRCTHNTH.Hhskp8eoff0vJKFQ/em5fbWOLYqFdlA8NJtHDGBbNEQ=';
        assert.deepEqual(actual, expected);
    });
});
describe('#signPUT(apiKey, time, url, bodySignature, secret)', function () {
    it('should return signature', function () {
        var actual = signPUT(apiKey, time, url, bodySignature, secret);
        var expected = '134654.1518619891.SCTDLGCDRCTHNTH.4YrajbQfcGKFk6BATWiMt/WN/eh0uhkswJkgTfbW7Co=';
        assert.deepEqual(actual, expected);
    });
});
describe('#parseAuthHeader(input)', function () {
    it('should parse headers without body signature', function () {
        var input = '134654.1518619891.iL1udpe1sHDzj3eGfK8HKUk/00GXt1eV812w9nHjwTU=';
        var actual = parseAuthHeader(input);
        assert.deepEqual(actual.apiKey, apiKey);
        assert.deepEqual(actual.time, time);
        assert.deepEqual(actual.signature, 'iL1udpe1sHDzj3eGfK8HKUk/00GXt1eV812w9nHjwTU=');
    });
    it('should parse headers with body signature', function () {
        var input = '134654.1518619891.SCTDLGCDRCTHNTH.Hhskp8eoff0vJKFQ/em5fbWOLYqFdlA8NJtHDGBbNEQ=';
        var actual = parseAuthHeader(input);
        assert.deepEqual(actual.apiKey, apiKey);
        assert.deepEqual(actual.time, time);
        assert.deepEqual(actual.bodySignature, bodySignature);
        assert.deepEqual(actual.signature, 'Hhskp8eoff0vJKFQ/em5fbWOLYqFdlA8NJtHDGBbNEQ=');
    });
    describe('#AuthHeaderFields.timeDelta(input)', function () {
        it('should return the time delta (positive)', function () {
            var input = '134654.1518619891.SCTDLGCDRCTHNTH.Hhskp8eoff0vJKFQ/em5fbWOLYqFdlA8NJtHDGBbNEQ=';
            var h = parseAuthHeader(input);
            var actual = h.timeDelta(time + 50);
            assert.deepEqual(actual, -50);
        });
        it('should return the time delta (negative)', function () {
            var input = '134654.1518619891.SCTDLGCDRCTHNTH.Hhskp8eoff0vJKFQ/em5fbWOLYqFdlA8NJtHDGBbNEQ=';
            var h = parseAuthHeader(input);
            var actual = h.timeDelta(time - 50);
            assert.deepEqual(actual, 50);
        });
    });
});
