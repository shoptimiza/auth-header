'use strict';
const crypto = require('crypto');
const headerKey = 'X-Shoptimiza-Auth';

function unixTime(offset) {
    return toUnixTime(Date.now()) + (offset || 0);
}

function toUnixTime(input) {
    return Math.floor(input.valueOf() / 1000);
}

function signGET(apiKey, time, url, secret) {
    return signParams([apiKey, time, 'GET', url], secret);
}

function signHEAD(apiKey, time, url, secret) {
    return signParams([apiKey, time, 'HEAD', url], secret);
}

function signPOST(apiKey, time, url, bodySignature, secret) {
    return signParams([apiKey, time, 'POST', url, bodySignature], secret);
}

function signPUT(apiKey, time, url, bodySignature, secret) {
    return signParams([apiKey, time, 'PUT', url, bodySignature], secret);
}

function signParams(params, secret) {
    var ret = '';
    const hmac = crypto.createHmac('sha256', secret);
    params[3] = params[3].replace(/^https?\:\/\//, ''); // remove http:// or https://
    const input = params.join('.');
    hmac.update(input);
    ret = params[0] + '.' + params[1];
    if (params.length === 5) {
        ret += '.' + params[4];
    }
    return ret + '.' + hmac.digest('base64');
}

function AuthHeaderFields(input) {
    var [apiKey, time, bodySignature, signature] = input.split('.');
    if (!signature) {
        signature = bodySignature;
        bodySignature = null;
    }
    this.apiKey = apiKey;
    this.time = +time;
    this.bodySignature = bodySignature;
    this.signature = signature;
    this.rawSignature = input;
}
AuthHeaderFields.prototype.timeDelta = function timeDelta(input) {
    return this.time - input;
};

function parseAuthHeader(input) {
    return new AuthHeaderFields(input);
}
module.exports = {
    signGET,
    signHEAD,
    signPOST,
    signPUT,
    headerKey,
    unixTime,
    toUnixTime,
    parseAuthHeader
};
