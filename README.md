[![pipeline status](https://gitlab.com/shoptimiza/auth-header/badges/master/pipeline.svg)](https://gitlab.com/shoptimiza/auth-header/commits/master)
[![coverage report](https://gitlab.com/shoptimiza/auth-header/badges/master/coverage.svg)](https://gitlab.com/shoptimiza/auth-header/commits/master)

# Shoptimiza Auth Header
Utility to create [Shoptimiza]('https://www.shoptimiza.com') authentication headers

## Install 
```
$ npm install shoptimiza-auth-header
```


## Usage 

```
const {
    headerKey,
    signGET,
    unixTime,
} = require('shoptimiza-auth-header');

var url = 'https://api.shoptimiza.com/something';

var headers = {};
headers[headerKey] = signGET(apiKey, unixTime(), url, sharedSecret);
```

For more examples check the tests
